import base64
import io
import os
import random
import uuid

import flask
from flask import request, jsonify, session, send_file
import hashlib
import jwt
import datetime
import json

# 对数据库操作
import pymongo

from flask_cors import CORS

client = pymongo.MongoClient("mongodb://mongo:27017/")

app = flask.Flask(__name__)

CORS(app, resources=r'/*')
app.secret_key = "qiuwyherdhkuvcszqawoiedhu"

db = client["tbdproj"]
UserCol = db["users"]
TaskCol = db["tasks"]
ImgsCol = db["imgs"]

secret_key = 'Tritium0041'
aes_Key = 'ECWOhQRHulelarhrlLa+BfDQrTECNCr6'


@app.route('/signin', methods=['POST', 'GET'])
def signin():
    if request.method == 'POST':
        data = request.get_json()
        username = data['username']
        password = data['password']
        # 对密码sha256加密
    password_sha256 = hashlib.sha256(password.encode('utf-8')).hexdigest()
    # 连接数据库
    query = {'username': username, 'password': password_sha256}
    result = UserCol.find_one(query)
    if result:
        # 分发JWT
        token = {
            'iss': 'Tritium',
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
            'iat': datetime.datetime.utcnow(),
            "data": {
                "username": username
            }
        }
        token = jwt.encode(token, app.secret_key, algorithm='HS256')
        return {'code': 200, 'status': 'success', 'msg': '登录成功', 'token': token}

    else:
        return {'code': 400, 'status': 'fail', 'msg': '用户名或密码错误'}


# 注册
@app.route('/signup', methods=['POST'])
def signup():
    if request.method == 'POST':
        data = request.get_json()
        username = data['username']
        password = data['password']
        # 对密码sha256加密
        password_sha256 = hashlib.sha256(password.encode('utf-8')).hexdigest()
        # 连接数据库
        query = {'username': username}
        result = UserCol.find_one(query)
        # 如果有结果
        if result:
            print(result)
            return {'code': 400, 'status': 'fail', 'msg': '用户名已存在'}
        else:
            add = {'username': username, 'password': password_sha256, 'balance': 100, "jobs_num": 0}
            UserCol.insert_one(add)
            return {'code': 200, 'status': 'success', 'msg': '注册成功'}


# 获取用户信息
@app.route('/userinfo', methods=['POST'])
def userinfo():
    if request.method == 'POST':
        token = request.get_json()['token']
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data'].get('username')
        # 防止sql注入
        # 连接数据库
        query = {'username': username}
        result = UserCol.find_one(query)
        # 如果用户存在则从对应表内获取用户账户内货币数量、交易记录
        if result:
            return {'code': 200, 'status': 'success', 'msg': '获取用户信息成功', 'username': username,
                    'result': result["balance"]}
        else:
            return {'code': 400, 'status': 'fail', 'msg': '未登录'}
    else:
        return {'code': 403}


# 交易
def trade(username, delta):
    # 防止sql注入
    # 连接数据库
    query = {'username': username}
    result = UserCol.find_one(query)
    # 如果用户存在则从对应表内获取用户账户内货币数量、交易记录
    if result:
        # 根据delta和数据库内的货币数量判断是否可以进行交易
        balance = result['balance']
        if int(delta) > 0:
            balance = int(balance) + int(delta)
        else:
            if int(balance) + int(delta) < 0:
                return False
            else:
                balance = int(balance) + int(delta)
        # 更新数据库
        update = {'$set': {'balance': balance}}
        UserCol.update_one(query, update)
        return True
    else:
        # 则用户不存在
        return False


@app.route('/upload_task', methods=['POST'])
def upload_task():
    if request.method == 'POST':
        task_name = str(uuid.uuid1())
        ##
        # task内的一些参数，仅供参考，随时添加:
        # model:模型名称
        # mode:任务类型(txt2img,img2img)
        # prompt:提示词
        # task_num:生成数量
        # seed:随机种子
        # step:重采样步数
        # sampler:采样器
        # img:原图，如果是img2img任务则需要传入原图
        # cfg_scale:提示词相关性
        # denoising:重绘幅度，img2img任务需要传入
        # 获取task
        data = request.get_json()
        task_detail = data['task_detail']
        task_num = int(data['task_num'])
        token = data['token']
        # 调用交易接口
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data'].get('username')
        delta = -1 * int(task_num)
        if trade(username, delta):
            pass
        else:
            return {'code': 400, 'status': 'fail', 'msg': '余额不足'}
        # 将task存入数据库
        add = {"task_name": task_name, "task_detail": task_detail, "task_num": task_num, "task_status": 0,
               "task_result": list(), "worker": list(), "owner": username}
        TaskCol.insert_one(add)
        # 返回成功信息
        data = {"code": 200, 'status': 'success', 'msg': 'upload task success'}
        return jsonify(data)
    else:
        return {'code': 403}


# 获取任务列表（可能准备弃用）
@app.route('/get_task_list', methods=['POST'])
def get_task_list():
    data = request.get_json()
    token = data['token']
    username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data'].get('username')
    if username is None:
        return {'code': 400, 'status': 'fail', 'msg': '未登录'}
    query = {'owner': username}
    result = TaskCol.find(query)
    taskList = []
    for task in result:
        taskList.append(task['task_name'])
    if result:
        return {'code': 200, 'status': 'success', 'msg': '获取任务列表成功', 'task_list': taskList}


# 认领任务
@app.route('/claim_task', methods=['POST'])
def claim_task():
    if request.method == 'POST':
        data = request.get_json()
        token = data['token']
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data'].get('username')
        # 从数据库中取出一个未完成的任务
        # 第三代获取任务算法
        query = {
            '$expr': {
                '$lt': [
                    '$task_status', '$task_num'
                ]
            }
        }
        result = TaskCol.find(query)
        print(result)
        if result:
            for task in result:
                print(task)
                available_num = task['task_num'] - task["task_status"]
                if available_num < 1:
                    pass
                else:
                    task_num = random.randint(1, available_num)
                    task_id = task['_id']
                    # 更新task_status
                    task_status = task['task_status']
                    task_status += task_num
                    query = {'_id': task_id}
                    update = {'$set': {'task_status': task_status}}
                    TaskCol.update_one(query, update)
                    # 设置task的worker，列表中添加tasknum个username
                    worker = task['worker']
                    print(worker)
                    for i in range(task_num):
                        worker.append(username)
                    # 将这个task的worker更新
                    update = {'$set': {'worker': worker}}
                    TaskCol.update_one(query, update)
                    # 返回任务信息
                    data = {"code": 200, 'status': 'success', 'msg': 'claim task success',
                            'task_name': task['task_name'],
                            'task_detail': task['task_detail'], 'task_num': task_num}
                    return jsonify(data)
            return {'code': 400, 'status': 'fail', 'msg': '没有可用任务'}
        else:
            return {'code': 400, 'status': 'fail', 'msg': '没有可用任务'}







        # query = {'task_status': {'$lt': '$task_num'}}
        # result = TaskCol.find(query)
        # if result:
        #     for task in result:
        #         if task['task_num'] - task["task_status"] < task_num:
        #             pass
        #         else:
        #             # 更新task_status
        #             task_status = task['task_status']
        #             task_status += task_num
        #             update = {'$set': {'task_status': task_status}}
        #             TaskCol.update_one(query, update)
        #             # 设置task的worker，列表中添加tasknum个username
        #             worker = task['worker']
        #             for i in range(task_num):
        #                 worker.append(username)
        #             # 将这个task的worker更新
        #             update = {'$set': {'worker': worker}}
        #             TaskCol.update_one(query, update)
        #             # 返回任务列表
        #             data = {"code": 200, 'status': 'success', 'msg': 'claim task success','task_num':task_num,
        #                     'task_name': task['task_name'], 'task_detail': task['task_detail']}
        #             return jsonify(data)

            # if result["task_num"]-result['task_status'] < task_num:
            #     return {'code': 400, 'status': 'fail', 'msg': '任务数量不足'}
            # else:
            #     #更新task_status
            #     task_status = result['task_status']
            #     task_status += task_num
            #     update = {'$set': {'task_status': task_status}}
            #     TaskCol.update_one(query, update)
            #     #设置task的worker，列表中添加tasknum个username
            #     worker = result['worker']
            #     for i in range(task_num):
            #         worker.append(session['username'])
            #     #更新数据库
            #     update = {'$set': {'worker': worker}}
            #     TaskCol.update_one(query, update)



# 任务完成提交
@app.route('/submit_task', methods=['POST'])
def submit_task():
    if request.method == 'POST':
        data = request.get_json()
        token = data['token']
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data']['username']
        # 获取task的id
        task_name = data['task_name']
        task_num = data['task_num']
        # base64编码过的图片
        pics_list = data['pics_list']
        # 将pics保存到本地
        for pic in pics_list:
            img = base64.b64decode(pic)
            with open('./imgs/' + task_name +"-"+hashlib.md5(pic.encode('utf-8')).hexdigest()+ '.jpg', 'wb') as f:
                f.write(img)
        # 图片保存进ImgsCol
        for pic in pics_list:
            picPath = "./imgs/" + task_name +"-"+hashlib.md5(pic.encode('utf-8')).hexdigest()+ '.jpg'
            pic = {"task_name": task_name, 'picPath': picPath, "md5": hashlib.md5(pic.encode('utf-8')).hexdigest()}
            ImgsCol.insert_one(pic)
            # 设置TaskCol中的Task_result
            query = {'task_name': task_name}
            result = TaskCol.find_one(query)
            task_result = result['task_result']
            task_result.append(pic['md5'])
            update = {'$set': {'task_result': task_result}}
            TaskCol.update_one(query, update)
        # 将worker列表中所有username改为finished
        query = {'task_name': task_name}
        TaskCol.update_one(query, {'$set': {'worker.$[elem]': 'finished'}},
                           array_filters=[{'elem': username}])
        # 通过交易接口将奖励发放给用户
        delta = task_num
        trade(username, delta)
        # 返回成功信息
        data = {"code": 200, 'status': 'success', 'msg': 'submit task success'}
        return jsonify(data)


# 获取任务结果
@app.route('/getPicsList', methods=['POST'])
def get_result():
    if request.method == 'POST':
        data = request.get_json()
        token = data['token']
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data']['username']
        # 获取task的name
        task_name = data['task_name']
        # 获取任务结果
        query = {'task_name': task_name}
        result = ImgsCol.find(query)
        md5_list = []
        for pic in result:
            md5_list.append(pic['md5'])
        # 返回结果
        data = {"code": 200, 'status': 'success', 'msg': 'get result success', 'md5': md5_list}
        return jsonify(data)
    else:
        return {'code': 403, 'status': 'fail', 'msg': '未登录'}

@app.route('/getPic', methods=['POST'])
def getPic():
    if request.method == 'POST':
        data = request.get_json()
        token = data['token']
        username = jwt.decode(token, app.secret_key, algorithms=['HS256'])['data']['username']
        md5 = data['md5']
        query = {'md5': md5}
        result = ImgsCol.find_one(query)
        picPath = result['picPath']
        with open(picPath, 'rb') as f:
            pic = f.read()
            return send_file(io.BytesIO(pic), mimetype='image/jpeg')
    else:
        return {'code': 403, 'status': 'fail', 'msg': '未登录'}






#检测服务端文件列表
# @app.route("/checkList",methods=['GET'])
# def checkList():
#     with open('FileList.json','r') as f:
#         fileList = json.load(f)
#         f.close()
#     return fileList



# 将所有接口的说明写成注释
# 注册
# 请求方式：POST
# 请求参数：username,password
# 返回参数：code,status,msg
# 登陆
# 请求方式：POST
# 请求参数：username,password
# 返回参数：code,status,msg
# 登出
# 请求方式：GET
# 请求参数：无
# 返回参数：code,status,msg
# 获取用户信息
# 请求方式：GET
# 请求参数：无
# 返回参数：code,status,msg,username,result
# 进行交易
# 请求方式：POST
# 请求参数：content
# content为一个json字符串，包含data和signature，data为一个json字符串，包含coin和delta，signature为签名
# 返回参数：code,status,msg

# 接入爱发电webhook
# @app.route('/rewyuihkjafskuewkrjafs/afd', methods=['POST'])
# def afd():
#     if request.method == 'POST':
#         data = request.get_json()
#         amount = int(data["data"]["order"]['total_amount'])
#         username = data["data"]["order"]['remark']
#         # 获取用户信息
#         # 获取交易金额
#         delta = 10 * amount
#         # 交易
#         result = trade(username, delta)['code']
#         # 返回成功信息
#         if result == 200:
#             data = {"ec": 200, 'status': 'success', 'msg': 'afd success', "username": username}
#         else:
#             data = {"ec": 400, 'status': 'fail', 'msg': 'afd fail', "username": username}
#         return jsonify(data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
