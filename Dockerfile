FROM python:3.11
LABEL maintainer="Tritium haichuanyu243@gmail.com"
WORKDIR /app
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
COPY app.py /app/
RUN mkdir imgs
CMD ["gunicorn","-w","4","-b","0.0.0.0:5005","app:app"]
